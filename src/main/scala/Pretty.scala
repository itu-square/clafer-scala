package org.clafer
import scala.language.implicitConversions

trait Pretty[T] {

  type Pretty[T] = T => String

  def pp[T]  (t :Option[T]) (implicit pp :Pretty[T]) =
    t.map(pp(_)) getOrElse ""
}
