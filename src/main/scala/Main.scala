import org.clafer.ast._
import org.clafer.PrettyClafer

object Examples {

  val clafer :Clafer = Clafer (
    name = "Clafer",
    abs = true,
    gen = None
  )

  val person :Clafer =
    Clafer (
      name = "Person",
      abs = true,
      constr = List(
        Constr (Some ("constraint01"), Ref ("Person")),
        Assert (Some ("assertion01"), Ref ("Sing"))),
      children = List (Clafer ("age", false, false, Some ("int")),
                       Clafer ("female", true, false, None, Some ("age"),
                                Some(Bound(0,-1)), Some(Xor)))
    )

  val int :Clafer =
    Clafer ("int", false, false)

  val sing :Clafer =
    Clafer (
      name = "Sing",
      children = List (clafer, int, person)
    )
}

object Main extends App { println (PrettyClafer.pp (Examples.sing)) }
