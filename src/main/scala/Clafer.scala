/** Abstract syntax model for Clafer */
package org.clafer.ast

trait Named { val name :String }
trait MaybeNamed { val name :Option[String] }

sealed trait Expr
case class Bop (l :Expr, r:Expr, op :String) extends Expr
case class Uop (e :Expr, op :String) extends Expr
case class Ref (c :String) extends Expr

sealed trait Pred extends MaybeNamed {
  val expr :Expr
}

case class Constr (name :Option[String], expr :Expr)
  extends Pred
case class Assert (name :Option[String], expr :Expr)
  extends Pred

sealed trait Card
case object Xor extends Card
case object Or  extends Card
case object Mux extends Card
case object Opt extends Card
case class Const(value :Int) extends Card {
  assert (value >= -1)
}
case class Bound (lower :Int, upper :Int) extends Card {
  assert (lower >= 0 && upper >= -1)
}

case class Clafer (
  name     :String,
  abs      :Boolean = false,
  bag      :Boolean = false,
  gen      :Option[String] = Some ("Clafer"),
  ref      :Option[String] = None,
  cmult    :Option[Card] = None,
  gcard    :Option[Card] = None,
  children :Seq[Clafer] = List[Clafer] (),
  constr   :Seq[Pred] = List () ) extends Named
