// active: incorporate unit testing. just complete tests for PP
// property testing
// add grammar and parsing (including an easy test interface)
// add alloy code generation
// add temporal features
// jak zrobic taką metodę parse jak w Xtext, żeby łatwo testować?
// String generation is inefficient, but  I valued simplicity and learning scala at this stage
// Use FunSpec for unit testing specification
// I should use FeatureSpec for acceptance testing, on the model level; like Rao's model test cases, or even our collection of test models
// we need quantifiers with bindings
package org.clafer

import org.clafer.ast._
import org.clafer.Pretty
import scala.language.implicitConversions

object PrettyClafer extends Pretty[Clafer] {

  implicit def pp (c :Card) = c match {
    case Xor          => "xor"
    case Or           => "or"
    case Mux          => "mux"
    case Opt          => "?"
    case Const (-1)   => "*"
    case Const (value)=> s"$value"
    case Bound (l,-1) => s"$l..*"
    case Bound (l,u)  => s"$l..$u"
  }

  implicit def pp (e :Expr) :String = e match {
    case Ref  (c) => c
    case Bop (l,r,op) => pp(l)+op+pp(r)
    case Uop (e,op) => op + pp(e)
  }

  implicit def pp (p :Pred) :String = p match {
    case Assert (Some (n),e) => s"assert $n [${pp(e)}]"
    case Assert (None, e)    => s"assert [${pp(e)}]"
    case Constr (Some (n),e) => s"fact $n [${pp(e)}]"
    case Constr (None, e)    => s"[${pp(e)}]"
  }

  implicit def pp (c:Clafer) :String = {

    def _pp (indent: String) (c :Clafer) :String = {

      val indent1 = indent + "  "
      val head_list = List (
          pp[Card] (c.gcard),
          if (c.abs) "abstract" else "",
          c.name,
          c.gen.map {":" + _} getOrElse "",
          c.ref.map {(if (c.bag) "->> " else "-> ") + _} getOrElse "",
          pp (c.cmult)
        )
      val head_line = indent + head_list.filter(!_.isEmpty).mkString(" ")
      val children = c.children.map (_pp (indent1) _) mkString ("\n")
      val constr =
            if (c.constr.isEmpty) ""
            else c.constr.map(pp(_)).mkString (indent1, "\n"+indent1, "")
      List(head_line, children, constr).filter(!_.isEmpty).mkString("\n")
    }

    _pp ("") (c)
  }

}
