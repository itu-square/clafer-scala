package Clafer

import org.scalatest._
import org.clafer.ast._
import org.clafer.PrettyClafer._

class PrettySpec extends UnitSpec {

  val clafer :Clafer = Clafer (
    name = "Clafer",
    abs = true,
    gen = None
  )

  val person :Clafer =
    Clafer (
      name = "Person",
      abs = true,
      constr = List(
        Constr (Some ("constraint01"), Ref ("Person")),
        Assert (Some ("assertion01"),Ref ("Sing") )),
      children = List (Clafer ("age", false, false, Some ("int")))
    )

  val int :Clafer =
    Clafer ("int", false, false)

  val sing :Clafer =
    Clafer (
      name = "Sing",
      children = List (clafer, int, person)
    )




  describe ("Card ") {
    describe ("when bound is -1") {
      it ("should be serialized as '*'") {
        assert (pp(Some(Const(-1))) === "*")
        assert (pp(Some(Bound(0,-1))) === '*')
      }
    }

    describe ("with natural bounds") {
      it ("should be pprinted verbatim") {
        assert (pp(Some(Const(153))) === "153")
        assert (pp(Some(Bound(12,153))) == "12..153")
      }
    }

    describe ("syntactic sugar") {
      it ("should be pprinted using keywords") {
         assert (pp(None     ) === ""   )
         assert (pp(Some(Xor)) === "xor")
         assert (pp(Some(Or )) === "or" )
         assert (pp(Some(Mux)) === "mux")
         assert (pp(Some(Opt)) === "?"  )
      }

    }
  }

  val e1 = Bop (l=Ref ("Person"), r=Ref ("Age"), op=".")
  val e2 = Bop (l=Ref ("Person"), r=Ref ("Age"), op="+")

  describe ("Expressions") {
    they ("should be serialized including all subexpressions") {
      assert (pp(e1) === "Person.Age")
      assert (pp(e2) === "Person+Age")
    }
    they ("should get automatic parentheses based on precedence ") {
      fail ()
    }
    describe ("with unary operations") {
      it ("should automatically select operator placement") {
        val e = Uop (Ref ("Person"), "!")
        assert (pp(e) === "!Person")
        fail ("many operator tests need to be implemented")
      }
    }
  }

  describe ("Asserts") {
    describe ("unlabeled") {
      it ("should appear with keyword, no label") {
         assert (pp(Assert(None,e1)) === "assert [Person.Age]")
      }
    }
    describe ("labeled") {
      it ("should appear with keyword and label") {
        assert (pp(Assert(Some ("l"),e2)) === "assert l [Person+Age]")
      }
    }

  }

  describe ("Pretty printing of clafers") {
    it ("should contain all properties")  {
      assert (pp(person).lines.next === "abstract Person :Clafer")
      fail ("more properties should be tested")
    }

    it ("should contain all clafer children") {
      fail ("TODO")
    }

    it ("should contain all clafer constraints") {
      fail ("TODO")
    }

  }


}
